# from bottle import route, run, template

import os
import functools
import re

import bottle

bottle.TEMPLATE_PATH = ["static", "views"]
app = bottle.default_app()

# use jinja2?
# https://buxty.com/b/2013/12/jinja2-templates-and-bottle/
# view = functools.partial(bottle.jinja2_view, template_lookup=['static', 'views'])



start_of_script = """
<script type="text/babel">

    var destination = document.querySelector("#entry");
"""

component_examples_data = {
    "clock": {"tutorials": ["https://facebook.github.io/react/docs/state-and-lifecycle.html"]},
    "todo-list": {"tutorials": ["https://www.kirupa.com/react/simple_todo_app_react.htm"]},
    "validated-form": {"tutorials": "http://jamesknelson.com/react-js-by-example-interacting-with-the-dom/"},
    "react-router-basic": {"tutorials": ["https://css-tricks.com/learning-react-router/"]},
}

def get_local_dirpath(name):
    return os.path.join(os.getcwd(), name)

VIEWS_DIR = get_local_dirpath('views')

def read_content(the_file):
    with open(the_file, 'r') as f:
        return f.read()

def get_page_names():
    raw_names = os.listdir(VIEWS_DIR)
    names = [name.replace('.tpl', '') for name in raw_names if name != 'base.tpl']
    return names

def lookup_template_filepath(name):
    # finds the path based on page name
    return os.path.join(VIEWS_DIR, name + ".tpl")

def lookup_tutorial(name):
    try:
        return component_examples_data[name]['tutorial']
    except:
        return None

def get_react_code(name):
    print(name)
    code = read_content(lookup_template_filepath(name))
    rgx = r"[%]\s*rebase[(].*?[)]"
    code = re.sub(rgx, "", code)
    code = start_of_script + code + "</script>"
    return code

def get_data(name=None):
    names = get_page_names()
    if not name:
        name = names[0]
    return {'names': names, 'code': get_react_code(name), 'tutorial': lookup_tutorial(name)}



@app.route('/')
def home():
    names = get_page_names()
    name = names[0]
    return bottle.redirect('/' + name)

@app.route('/<name>')
def router(name):
    names = get_page_names()
    if name in names:
        code = get_react_code(name)
        tutorial = lookup_tutorial(name)
        return bottle.template(name, name=name, **get_data(name))
    else:
        print("The file {0} was ignored by the entry router".format(name))
        return "<h1>Could not find a file at {0}".format(name)

# send static
# http://stackoverflow.com/questions/10486224/bottle-static-files/37691570
@app.get('/static/ace-builds2/<filename:path>')
def send_static(filename):
    return bottle.static_file(filename, root='static/ace-builds2')

# reloader restarts server on change; debug refreshes page on template changes
app.run(host="0.0.0.0", port=8001, reloader=True, debug=True)
