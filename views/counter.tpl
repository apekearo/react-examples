% rebase('base.tpl', name=name, names=names)

class Counter extends React.Component {
    constructor() {
        super();
        this.state = { value: 0};
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.reset = this.reset.bind(this);
    }

  increment(){
    this.setState(prevState => ({
      value: prevState.value + 1
    }));
  };

  decrement(){
    this.setState(prevState => {
      var deducted = prevState.value - 1;
      if (deducted < 0){
          return { value: 0};
      } else{
          return { value: prevState.value - 1 }
      }
    });
  };

  reset(){
      this.setState({ value: 0 });
  }

  add(){

  }

  render() {
    return (
      <div id="main">
        {this.state.value}
        <button className="counter-button" onClick={this.increment}>+</button>
        <button className="counter-button" onClick={this.decrement}>-</button>
        <button className="counter-button" onClick={this.reset}>Reset</button>
      </div>
    )
  }
}

ReactDOM.render(
    <div>
        <Counter/>
    </div>,
    destination
)
